package main.test;

import main.java.ComparatorIntegerImplementation;
import main.java.CustomNumberEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ComparatorTest {

    ComparatorIntegerImplementation comparatorIntegerImplementation;
    CustomNumberEntity customNumberEntity;
    @BeforeEach
    public void beforeEach(){
        comparatorIntegerImplementation = new ComparatorIntegerImplementation();
    }

    @Test
    public void searchForIntegerValueFromFileTest(){
        boolean dataUnderTest = comparatorIntegerImplementation.executeComparison(67, "sampleNumberData.json");

        assertEquals(dataUnderTest, true);

    }

    @Test
    public void searchForIntegerValueWihNullTest(){
        boolean dataUnderTest = comparatorIntegerImplementation.contains(12, listOfNullValueCustomNumberEntity());

        assertEquals(dataUnderTest, false);
    }

    @Test
    public void searchForIntegerValueWihStringTest(){
        boolean dataUnderTest = comparatorIntegerImplementation.contains(12, listOfNonNumericValueCustomNumberEntity());

        assertEquals(dataUnderTest, false);
    }

    private List<CustomNumberEntity> listOfNullValueCustomNumberEntity(){
        List<CustomNumberEntity> customNumberEntitiesLst = new ArrayList<>();
        customNumberEntitiesLst.add(null);

        return customNumberEntitiesLst;
    }

    private List<CustomNumberEntity> listOfNonNumericValueCustomNumberEntity(){
        List<CustomNumberEntity> customNumberEntitiesLst = new ArrayList<>();
        CustomNumberEntity customNumberEntity = new CustomNumberEntity();
        customNumberEntity.setNumber("");
        customNumberEntity.setNumber("");
        customNumberEntity.setNumber("");
        customNumberEntity.setNumber("");

        return customNumberEntitiesLst;
    }
}
