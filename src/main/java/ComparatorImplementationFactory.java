package main.java;

/**
 * This Factory allows the application to scale and handle other primitive types of search.
 */
public abstract class ComparatorImplementationFactory {

    public boolean executeComparison(int value, String filePath) {
        return false;
    }

    public boolean executeComparison(String value, String filePath) {
        return false;
    }

    public boolean executeComparison(char value, String filePath) {
        return false;
    }


}
