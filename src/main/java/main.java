package main.java;

import java.util.Scanner;
import java.util.regex.Pattern;

public class main{


    public static void main(String[] args) {
        ComparatorIntegerImplementation comparatorIntegerImplementation = new ComparatorIntegerImplementation();
        boolean validInputFlag = false;
        Scanner myObj = new Scanner(System.in);  // Create a Scanner object
        System.out.println("Enter value you wish to search for");
        String inputValue = myObj.nextLine();  // Read user input

        while(!validInputFlag){
            if(isNumeric(inputValue)){
                //Hard Coded the filePath of the JSON data to make it easier to run the application
                System.out.println(comparatorIntegerImplementation.executeComparison(Integer.parseInt(inputValue), "sampleNumberData.json"));

                System.out.println("Would you like to search again? yes/no");
                String repeat = myObj.nextLine();

                if(repeat.equalsIgnoreCase("no")){
                    System.out.println("Thank you!!");
                    validInputFlag = true;
                }else{
                    System.out.println("Enter value you wish to search for");
                    inputValue = myObj.nextLine();  // Read user input}
                }
            }else {System.out.println("This functionality has not been implemented yet. Please enter a numeric value");
                inputValue = myObj.nextLine();}
        }
    }

    private static boolean isNumeric(String numberEntityLst) {
        Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");
        return pattern.matcher(numberEntityLst).matches();
    }
}
