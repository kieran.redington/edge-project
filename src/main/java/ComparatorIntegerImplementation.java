package main.java;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.regex.Pattern;

public class ComparatorIntegerImplementation extends ComparatorImplementationFactory implements NumberFinder{

    FastestComparator fastestComparator;

    public boolean executeComparison(int value, String filePath){
        System.out.println("Searching....");

        return contains(value, readFromFile(filePath));
    }


    @Override
    public boolean contains(int valueToFind, List<CustomNumberEntity> numberEntityLst) {
        fastestComparator = new FastestComparator();

        return processSearch(valueToFind, numberEntityLst);
    }

    /**
     * As the compare method has a delay built in I have removed any non numeric and duplicate values from the CustomNumberEntity list.
     * The isNumeric() method strips down the List to only valid numeric values.
     **/
    private boolean processSearch(int valueToFind, List<CustomNumberEntity> numberEntityLst) {
        //Commenting out this. I was looking at using multitasking to improve query time.
       /* List<CustomNumberEntity> subSets = isNumeric(numberEntityLst).subList(((isNumeric(numberEntityLst).size() - isNumeric(numberEntityLst).size()/4)), (isNumeric(numberEntityLst).size()));
        List<CustomNumberEntity> subSets1 = isNumeric(numberEntityLst).subList((isNumeric(numberEntityLst).size() - isNumeric(numberEntityLst).size()/2), isNumeric(numberEntityLst).size() - isNumeric(numberEntityLst).size()/4);
        List<CustomNumberEntity> subSets2 = isNumeric(numberEntityLst).subList((isNumeric(numberEntityLst).size()/4), (isNumeric(numberEntityLst).size()/2));
        List<CustomNumberEntity> subSets3 = isNumeric(numberEntityLst).subList((0), (isNumeric(numberEntityLst).size()/4));

        Thread t1 = new Thread(String.valueOf(checkValue(valueToFind, subSets)));
        Thread t2 = new Thread(String.valueOf(checkValue(valueToFind, subSets1)));
        Thread t3 = new Thread(String.valueOf(checkValue(valueToFind, subSets2)));
        Thread t4 = new Thread(String.valueOf(checkValue(valueToFind, subSets3)));
*/

        return  isNumeric(numberEntityLst)
                .stream()
                .anyMatch(entry -> fastestComparator.compare(valueToFind, entry) == 0);
    }

    @Override
    public List<CustomNumberEntity> readFromFile(String filePath) {
        JSONParser parser = new JSONParser();
        List<CustomNumberEntity> customNumberEntitiesLst = new ArrayList<>();
        CustomNumberEntity customNumberEntity;
        JSONArray incomingJSON;
        try {
            incomingJSON = (JSONArray) parser.parse(new FileReader(filePath));
            for (Object JSONNodeValue : incomingJSON)
            {
                JSONObject person = (JSONObject) JSONNodeValue;
                customNumberEntity = new CustomNumberEntity();
                String name = (String) person.get("number");
                customNumberEntity.setNumber(name);
                customNumberEntitiesLst.add(customNumberEntity);

            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        return customNumberEntitiesLst;
    }

    private List<CustomNumberEntity> isNumeric(List<CustomNumberEntity> numberEntityLst) {
        Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");

        List<CustomNumberEntity> filteredList = new ArrayList<>();

        try{
            for (CustomNumberEntity value : numberEntityLst) {
                if (value.getNumber() != null && !filteredList.contains(value)) {
                    if (pattern.matcher(value.getNumber()).matches()) {
                        filteredList.add(value);
                    }
                }
            }
        }catch (NullPointerException  e){
            System.out.println("The CustomNumberEntity has no values: " +e);
        }
        return filteredList;
    }
}
